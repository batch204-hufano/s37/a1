const jwt = require('jsonwebtoken');

const secret = 'CourseBookingAPI';

// JSON Web Tokens

// Toke Creation
module.exports.createAccessToken = (user) => {

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	// Generate a token
		// jwt.sign()
	return jwt.sign(data, secret, {})
}

// 
