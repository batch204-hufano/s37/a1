const express = require('express');
const router = express.Router();

const userController = require('../controllers/userController')

//Route for checking if the user's email already exist in database
router.post('/checkEmail', (request, response) => {

	userController.checkEmailExists(request.body).then(resultFromController => response.send(resultFromController));
});

// Route for User Registration
router.post('/register', (request, response) => {

	userController.registerUser(request.body).then(resultFromController => response.send(resultFromController));
});


// User Authentication
router.post('/login', (request, response) => {
	userController.loginUser(request.body).then(resultFromController => response.send(resultFromController))
});


//Create a /details route that will accept the user’s Id to retrieve the details of a user.
router.post('/details' , (request, response) => {
	userController.getProfile(request.body).then(resultFromController => response.send(resultFromController))
})

module.exports = router;