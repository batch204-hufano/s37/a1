const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth')
// Check if email exists
module.exports.checkEmailExists = (requestBody) => {

	return User.find({email: requestBody.email}).then(result => {

		if(result.length > 0) {
			return result
		} else {
			return false
		}
	})
}


// Controller for User Registration
module.exports.registerUser = (requestBody) => {

	let newUser = new User({

		firstName: requestBody.firstName,
		lastName: requestBody.lastName,
		email: requestBody.email,
		mobileNo: requestBody.mobileNo,
		// bcrypt.hashSync(myPlaintextPassword, saltRounds);
		password: bcrypt.hashSync(requestBody.password, 10)
	})

	return newUser.save().then((user, error) => {

		if(error) {
			return false
		} else {
			return true
		}
	})
}


// User Authentication
module.exports.loginUser = (requestBody) => {

	return User.findOne({email: requestBody.email}).then(result => {

		if(result == null) {
			return false
		} else {

			const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password);


			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}

// Create a getProfile controller method for retrieving the details of the user:
module.exports.getProfile = (requestBody) => {

	return User.findById(requestBody.id).then(result => {

		result.password = "";
		return result

	})
}