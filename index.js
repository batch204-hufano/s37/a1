const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const port = 4000;

const userRoutes = require('./routes/userRoutes');

const app = express();

// Connect to our MongoDB Database
mongoose.connect("mongodb+srv://admin:admin123@batch204-hufano.2rq00td.mongodb.net/s37-s41?retryWrites=true&w=majority", 
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);

let db = mongoose.connection;

//If a connection error occured, output in the console
db.on("error", console.error.bind(console, "Error"));

//If the connection is successful, output in the console
db.once("open", () => console.log("Now connected to MongoDB Atlas!"));

// Middleware
app.use(express.json());
app.use(cors());

app.use('/users', userRoutes);

app.listen(port, () => {
	console.log(`API is now online on port ${port}`)
})